
package Pbo2.NIdya110;

abstract class Penduduk {
    protected String nama, tanggalLahir;

    public Penduduk() {
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    
    abstract public double hitungIuran();
}

