package Pbo2.NIdya110;

import java.util.Scanner;

public class MasyarakatSekitar extends Penduduk implements Peserta {

    private String nomor;

    public MasyarakatSekitar() {
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    @Override
    public double hitungIuran() {
        String nomor2 = nomor.substring(0, 3);
        return Long.parseLong(nomor2) * 100;

    }

    public void inputDataMsy() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama Masyarakat : ");
        nama = sc.nextLine();
        System.out.print("Nomor\t\t : ");
        nomor = sc.nextLine();
        System.out.print("Tgl Lahir       : ");
        tanggalLahir = sc.nextLine();
    }

    @Override
    public String jenisSertifikat() {
        return "Peserta";
    }

    @Override
    public String getFasilitas() {
        return "Block Note, Alat Tulis, Modul Pelatihan";
    }

    @Override
    public String getKonsumsi() {
        return "Snack dan Makasn Siang";
    }
}
