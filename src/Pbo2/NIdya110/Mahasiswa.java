package Pbo2.NIdya110;

import java.util.Scanner;

public class Mahasiswa extends Penduduk implements Peserta {

    private String nim;

    public Mahasiswa() {
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    @Override
    public double hitungIuran() {
        long nim2 = Long.parseLong(nim);
        return nim2 / 10000;
    }

    public void inputDataMhs() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama Mahasiswa   : ");
        nama = sc.nextLine();
        System.out.print("NIM\t\t : ");
        nim = sc.nextLine();
        System.out.print("Tgl Lahir        : ");
        tanggalLahir = sc.nextLine();
    }

    public void cetakDataMhs() {
        System.out.print("Nama Mahasiswa   : " + nama);
        System.out.print("NIM\t\t : " + nim);
    }

    @Override
    public String jenisSertifikat() {
        return "Panitia";
    }

    @Override
    public String getFasilitas() {
        return "Block Note, Alat Tulis, Laptop";
    }

    @Override
    public String getKonsumsi() {
        return "Snack, Makasn Siang, Makan Malam";
    }
}
