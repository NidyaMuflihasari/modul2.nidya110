package Pbo2.NIdya110;

import java.util.Scanner;

public class UKM {

    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private Penduduk[] anggota;
    private int maxMhs = 1000, lastIndexMhs;
    private Mahasiswa[] arrayMhs = new Mahasiswa[maxMhs];
    private int maxMsy = 1000, lastIndexMsy;
    private MasyarakatSekitar[] arrayMsy = new MasyarakatSekitar[maxMsy];
    public double totalIuranMsy = 0;
    public double totalIuranMhs = 0;

    public UKM() {
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public void setSekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public Penduduk[] getAnggota() {
        return anggota;
    }

    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }

    public void inputDataUKM() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama Unit\t : ");
        namaUnit = sc.nextLine();
    }

    public void cetakDataUKM() {
        System.out.println("Nama Unit : "+getNamaUnit());
    }
}
