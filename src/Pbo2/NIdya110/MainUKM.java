package Pbo2.NIdya110;

import java.util.Scanner;

public class MainUKM {

    static int pilih1, pilih2, banyakPen, counterPen = 0;
    static Scanner sc = new Scanner(System.in);
    static Penduduk[] anggota;
    static UKM ukm = new UKM();
    static Mahasiswa ketua = new Mahasiswa();
    static Mahasiswa sekretaris = new Mahasiswa();
    static double totalIuranAnggota = 0;

    public static void main(String[] args) {
        ukm.inputDataUKM();
        System.out.println("Masukan Data Ketua ");
        ketua.inputDataMhs();
        ukm.setKetua(ketua);
        System.out.println("Masukan Data Sekretaris");
        sekretaris.inputDataMhs();
        ukm.setSekretaris(sekretaris);
        do {
            System.out.print("Masukan Banyak Anggota   : ");
            banyakPen = sc.nextInt();
            System.out.println();
        } while (banyakPen <= 0);
        System.out.println("Data\t UKM\t " + ukm.getNamaUnit());
        System.out.println("1. Mahasiswa");
        System.out.println("2. Masyarakat Sekitar");
        anggota = new Penduduk[banyakPen];
        for (int i = 0; i < banyakPen; i++) {
            System.out.println("Masukan Data Anggota ke-" + (i + 1));
            do {
                System.out.print("Pilih Asal Anggota : ");
                pilih1 = sc.nextInt();
            } while (pilih1 <= 0 && pilih1 > 2);
            if (pilih1 == 1) {
                System.out.println("Anggota berasal dari mahasiswa");
                Mahasiswa mhs = new Mahasiswa();
                mhs.inputDataMhs();
                anggota[i] = mhs;
            } else if (pilih1 == 2) {
                System.out.println("Anggota berasal dari masyarakat sekitar");
                MasyarakatSekitar msy = new MasyarakatSekitar();
                msy.inputDataMsy();
                anggota[i] = msy;
            }
            System.out.println();
        }
        ukm.setAnggota(anggota);
        System.out.println("Data UKM" + ukm.getNamaUnit());
        System.out.print("============================================================");
        System.out.print("============================================================");
        System.out.println("==========================================================");
        System.out.println("No.\t NIM/NIK\t\t Nama\t\t Tanggal Lahir\t\t Asal\t\t "
                + "Iuran\t\t Sertifikat\t Fasilitas\t\t\t\t\t Konsumsi");
        System.out.print("============================================================");
        System.out.print("============================================================");
        System.out.println("==========================================================");
        for (int i = 0; i < banyakPen; i++) {
            System.out.print((i + 1) + "\t");
            if (i == 0) {
                System.out.print(ukm.getKetua().getNim() + "\t\t");
                System.out.print(ukm.getKetua().getNama() + "\t\t");
                System.out.print(ukm.getKetua().getTanggalLahir() + "\t\t");
                System.out.print("Mahasiswa\t");
                System.out.print("-\t\t");
                System.out.print(ukm.getKetua().jenisSertifikat() + "\t\t");
                System.out.print(ukm.getKetua().getFasilitas() + "\t\t");
                System.out.println(ukm.getKetua().getKonsumsi() + "\t\t");
            } else if (i == 1) {
                System.out.print(ukm.getSekretaris().getNim() + "\t\t");
                System.out.print(ukm.getSekretaris().getNama() + "\t\t");
                System.out.print(ukm.getSekretaris().getTanggalLahir() + "\t\t");
                System.out.print("Mahasiswa\t");
                System.out.print("-\t\t");
                System.out.print(ukm.getSekretaris().jenisSertifikat() + "\t\t");
                System.out.print(ukm.getSekretaris().getFasilitas() + "\t\t");
                System.out.println(ukm.getSekretaris().getKonsumsi() + "\t\t");
            }
        }
        for (int i = 0; i < banyakPen; i++) {
            System.out.print((i + 3) + "\t");
            if (anggota[i] instanceof Mahasiswa) {
                Mahasiswa mhs = (Mahasiswa) anggota[i];
                System.out.print(mhs.getNim() + "\t\t");
                System.out.print(mhs.getNama() + "\t\t");
                System.out.print(mhs.getTanggalLahir() + "\t\t\t");
                System.out.print("Mahasiswa\t");
                System.out.print(mhs.hitungIuran() + "\t\t");
                System.out.print(mhs.jenisSertifikat() + "\t\t");
                System.out.print(mhs.getFasilitas() + "\t\t");
                System.out.print(mhs.getKonsumsi() + "\t\t");
                System.out.println();
            } else if (anggota[i] instanceof MasyarakatSekitar) {
                MasyarakatSekitar mys = (MasyarakatSekitar) anggota[i];
                System.out.print(mys.getNomor() + "\t\t\t");
                System.out.print(mys.getNama() + "\t\t");
                System.out.print(mys.getTanggalLahir() + "\t\t");
                System.out.print("Masyarakat\t");
                System.out.print(mys.hitungIuran() + "\t\t");
                System.out.print(mys.jenisSertifikat() + "\t\t");
                System.out.print(mys.getFasilitas() + "\t\t");
                System.out.print(mys.getKonsumsi());
                System.out.println();
            }
            totalIuranAnggota = totalIuranAnggota + anggota[i].hitungIuran();
        }
        System.out.print("============================================================");
        System.out.print("============================================================");
        System.out.println("==========================================================");
        System.out.println();
        System.out.println("Total Iuran Anngota" + totalIuranAnggota);
    }
}
